C++ Extended Enum
=================
Headers allowing runtime checking of value, getting all values and their text name.

Compiling
---------
Compiler and its parameters used during development:
```bash
g++ -std=c++11 -Wall -Werror -pedantic
```

Usage
-----
The test could be used as an example of how the header works.

The header is controlled by the following macros (and several internal ones):
- `EXT_ENUM_NAME` = type name of enum
- `EXT_ENUM_TYPE` = backing type of enum (optional, default is `int`)
- `EXT_ENUM_SCOPED` = if defined, the enum will be a scoped one (e.g. `enum class ...`), otherwise an unscoped one will be introduced (e.g. `enum ...`)
- `EXT_ENUM_DATA` = macro containing members defined as `EXT_ENUM_DEF(<key>, <val>)` macro<br>
  If you want the default numbering, you could define the macro as `EXT_ENUM_DEF(<key>,)`.
  And if you want to change it, you have to include the equal sign in the definition `EXT_ENUM_DEF(<key>, =<value>)`.
  There should not be anything else than whitespaces between the members (no commas, semicolons, etc.).

- `EXT_ENUM_KEYS_LENGTH_TYPE` = return-type of keysLength helper function (optional, default is `unsigned char`)

Next, you should select the features you want to generate:
- `EXT_ENUM_GENALL` = shortcut to generate all features
- `EXT_ENUM_GEN_CHECK` = generate a function `static bool <enum-name>_check(<enum-name> key);`
- `EXT_ENUM_GEN_NAME` = generate a function `static const char* <enum-name>_name(<enum-name> key);`
- `EXT_ENUM_GEN_KEYS` = generate a function `static const <enum-name>* <enum-name>_keys();`<br>
  and its length helper `static const <length-type> <enum-name>_keysLength();`

Additionally, if a macro `EXT_ENUM_PRAGMAS` is defined, the generated functions will be surrounded by pragmas to disable warnings about unused functions.

All the defined macros will be undefined after generating all features.
If you want to keep them, define also the macro `EXT_ENUM_KEEP`.
This does not apply to internal macros which will be always undefined.

After all macros defined, include the header:
```c++
#include "ext_enum.h"
```

Example
-------
The source code (part of the test file):
```c++
#define EXT_ENUM_NAME Color
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_DATA \
	EXT_ENUM_DEF(green,) \
	EXT_ENUM_DEF(red, =3) \
	EXT_ENUM_DEF(blue,)
#define EXT_ENUM_GENALL
#include "ext_enum.h"
```

It will functionally results in the following preprocessed code:
```c++
enum Color : unsigned char {
	green,
	red = 3,
	blue
};

static bool Color_check(Color key){
	switch(key){
		case Color::green:
		case Color::red:
		case Color::blue:
			return true;
	default:
		return false;
	};
}

static const char* Color_name(Color key){
	switch(key){
		case Color::green:
			return "green";
		case Color::red:
			return "red";
		case Color::blue:
			return "blue";
		default:
			return nullptr;
	};
}

static const unsigned char* Color_keys(){
	static const unsigned char keys[] = {
		Color::green,
		Color::red,
		Color::blue
	};
	
	return keys;
}

static const unsigned char Color_keysLength(){
	return +1 +1 +1;
}
```

License
-------
This program is licensed under the MIT License.

See file [LICENSE](LICENSE).
