/**
***********************************************
***            C++ Extended Enum            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


#define EXT_ENUM_NAME Color
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	EXT_ENUM_DEF(green,) \
	EXT_ENUM_DEF(red, =3) \
	EXT_ENUM_DEF(blue,)
//#define EXT_ENUM_KEYS_LENGTH_TYPE unsigned int
#define EXT_ENUM_GENALL
//#define EXT_ENUM_GEN_CHECK
//#define EXT_ENUM_GEN_NAME
//#define EXT_ENUM_GEN_KEYS
#define EXT_ENUM_PRAGMAS
//#define EXT_ENUM_KEEP
#include "ext_enum.h"


// check if EXT_ENUM_KEEP is working
#ifdef EXT_ENUM_DATA
	#warning "Still data"
#endif
