/**
***********************************************
***            C++ Extended Enum            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


// test presence of basic helpers
#if !defined(EXT_ENUM_NAME) || !defined(EXT_ENUM_DATA)
	#error "Enum name or data not defined!"
#endif


// default underlying type
#ifndef EXT_ENUM_TYPE
	#define EXT_ENUM_TYPE int
#endif

// default keys-length type
#ifndef EXT_ENUM_KEYS_LENGTH_TYPE
	#define EXT_ENUM_KEYS_LENGTH_TYPE unsigned char
#endif


// simplify generation of all features
#ifdef EXT_ENUM_GENALL
	#define EXT_ENUM_GEN_CHECK
	#define EXT_ENUM_GEN_NAME
	#define EXT_ENUM_GEN_KEYS
#endif


// workaround for preprocessor to apply concatenation after NAME macro expansion
#define EXT_ENUM_NAMECAT_INNER( classname, methodname ) \
	classname  ## _ ## methodname
#define EXT_ENUM_NAMECAT( classname, methodname ) \
	EXT_ENUM_NAMECAT_INNER(classname, methodname)


/**
 * Enum definition
 */
#define EXT_ENUM_DEF(KEY, VALUE) KEY VALUE,
enum
#ifdef EXT_ENUM_SCOPED
	class
#endif
EXT_ENUM_NAME : EXT_ENUM_TYPE {
	EXT_ENUM_DATA
};
#undef EXT_ENUM_DEF
// enum


// ===== Avoid unused function warnings =====
#ifdef EXT_ENUM_PRAGMAS
	#ifdef __GNUC__
		#pragma GCC diagnostic push
		#pragma GCC diagnostic ignored "-Wunused-function"
	#endif
#endif


/**
 * Checking existence of member at runtime
 * @param key - value to check for membership
 * @return true if value is a member
 */
#ifdef EXT_ENUM_GEN_CHECK
	#define EXT_ENUM_DEF(KEY, VALUE) case EXT_ENUM_NAME::KEY:
	static bool EXT_ENUM_NAMECAT(EXT_ENUM_NAME, check)(EXT_ENUM_NAME key){
		switch(key){
			EXT_ENUM_DATA
				return true;
			default:
				return false;
		};
	}
	#undef EXT_ENUM_DEF
#endif
// check

/**
 * Getter for member name
 * @param key - value to get name
 * @return member name or null-pointer if the given value is not a member
 */
#ifdef EXT_ENUM_GEN_NAME
	#define EXT_ENUM_DEF(KEY, VALUE) case EXT_ENUM_NAME::KEY: return #KEY;
	static const char* EXT_ENUM_NAMECAT(EXT_ENUM_NAME, name)(EXT_ENUM_NAME key){
		switch(key){
			EXT_ENUM_DATA
			default:
				return nullptr;
		};
	}
	#undef EXT_ENUM_DEF
#endif
// name

/**
 * Return all members
 * @return array of all members
 */
#ifdef EXT_ENUM_GEN_KEYS
	#define EXT_ENUM_DEF(KEY, VALUE) EXT_ENUM_NAME::KEY,
	static const EXT_ENUM_NAME* EXT_ENUM_NAMECAT(EXT_ENUM_NAME, keys)(){
		static const EXT_ENUM_NAME keys[] = {
			EXT_ENUM_DATA
		};
		
		return keys;
	}
	#undef EXT_ENUM_DEF
#endif
// keys

/**
 * Return length of all members array
 * @return length
 */
#ifdef EXT_ENUM_GEN_KEYS
	#define EXT_ENUM_DEF(KEY, VALUE) +1
	static const EXT_ENUM_KEYS_LENGTH_TYPE EXT_ENUM_NAMECAT(EXT_ENUM_NAME, keysLength)(){
		return EXT_ENUM_DATA;
	}
	#undef EXT_ENUM_DEF
#endif
// keysLength


// ===== Restore settings =====
#ifdef EXT_ENUM_PRAGMAS
	#ifdef __GNUC__
		#pragma GCC diagnostic pop
	#endif
#endif


// always undefine all internal helpers
#undef EXT_ENUM_NAMECAT
#undef EXT_ENUM_NAMECAT_INNER

// undefine all helpers
#ifndef EXT_ENUM_KEEP
	#undef EXT_ENUM_NAME
	#undef EXT_ENUM_TYPE
	#undef EXT_ENUM_SCOPED
	#undef EXT_ENUM_DATA
	
	#undef EXT_ENUM_KEYS_LENGTH_TYPE
	
	#undef EXT_ENUM_GENALL
	#undef EXT_ENUM_GEN_CHECK
	#undef EXT_ENUM_GEN_NAME
	#undef EXT_ENUM_GEN_KEYS
	
	#undef EXT_ENUM_PRAGMAS
#endif
